import { MockClient, MockClientMessage } from "../../bot/mocks"
import { Command, CommandClass } from "../classes"

export const MockCommand: CommandClass<MockClientMessage, MockClient> = Command
